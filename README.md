﻿Contexte 
-------------------
Nous sommes en charge du développement d'un système de gestion pour une bilbiothèque municipale qui met à dispositions diverses ressources. 

Objet 
--------------------
La finalité de ce système est de faciliter l'accès aux diverses fonctionnalités aux différentes utilisateurs, que cela soit les adhérents ou les membres du personnel, mais aussi la gestion des documents et ressources mis à disposition tout en recueillant des données sur ces documents. <br>
Ces données serviront à établir des statistiques sur les documents empruntés, et permettront d'établir la liste des documents populaires, mais aussi d'étudier le profil des adhérents pour pouvoir leur suggérer des documents.

Acteurs 
--------------------
**Maîtrise d'ouvrage** : Chargé de TD de l'UV NF17 à l'UTC, Alessandro Victorino 

**Maîtrise d'oeuvre** : MEYER Florent,
DAM Sebastien,
KERJEAN Victor,
VAST Mathias

Contraintes 
--------------------
La maîtrise d'ouvrage exige que la gestion du projet se fasse sous Git, en particulier sur le serveur Gitlab de l'UTC. <br>
Les ressources du projet seront alors accessibles sous la forme d'une URL vers ce serveur Gitlab.  
Les bases de données relationnelles doivent être réalisées avec PostgreSQL, le code doit être compatible avec la version installée sur les serveurs de l'UTC.<br>
Les formats utilisés sont ceux imposés par la maîtrise d'ouvrage (*markdown*/*plaintext*)

Livrables 
-------------------
- **README.md**: un fichier comportant le nom des auteurs ainsi que toutes les informations nécessaires à la compréhension de l'architecture du projet. 
- **NDC**: Note de clarification au format markdown.
- **MCD**: un modèle UML au format *plantuml*
- **MLD Relationnel**: un modèle logique au format *plain text*
- **BDD**: la base de données détaillant la construction des tables, vues, données de test et questions attendues (vues)

Les livrables sont les fichiers les plus récents de la branche master (la version la plus récente étant caractérisée par le nom du fichier suivi de 2). Nous avons souhaité conserver 
les différentes versions afin de marquer l'évolution du projet.