Note de clarification : Biblio
======================

Nous sommes en charge du développement d'un système de gestion pour une bilbiothèque municipale qui met à dispositions diverses ressources. La finalité de ce système est de faciliter l'accès aux diverses fonctionnalités aux différentes utilisateurs, que cela soit les adhérents ou les membres du personnel, mais aussi la gestion des documents et ressources mis à disposition tout en recueillant des données sur ces documents. En termes de fonctionnalité, cela se traduira pour chaque utilisateur de la manière suivante :

- Coté adhérent/utilisateur lambda :
	- Le droit de consulter les ressources disponibles
	- Le droit d'emprunter, si c'est possible une oeuvre
	- Le droit de rendre un document qu'ils ont emprunté
	- Consulter l'état de leurs emprunts
	- Consulter l'état de leur compte utilisateur
	- Modifier leurs données (email, login, mdp)
- Coté membre du personnel :
	- Le droit de modifier les tables du système
	- Le droit d'ajouter des éléments dans les tables
	- Le droit de consulter les données des utilisateurs
	- Le droit de modifier les droits des utilisateurs (blacklister ou pénaliser)
	- Le droit de consulter l'état des emprunts en cours
	- Le droit de consulter les statistiques qui seront calculées à partir des données récoltées

La liste des objets :
-------------------------------
Ce système de gestion comprendra :
- **Ressource** :
	- code {integer} {key}
	- titre {string}
	- dateApparition {date}
	- editeur {string}
	- genre {string}
	- codeClassifiation {string}

Parmi les **Ressource**, on distinguera trois objets :
- Livre (sous classe de Ressource) :
	- ISBN {string} {local key}
	- resume {string}
	- langue {string}
- Film (sous classe de Ressource) :
	- langue {string}
	- longueur {integer}, on pourra choisir de la donner en minutes pour conserver le même format qu'avec les oeuvres musicales
	- synopsis {string}
- OeuvreMusicale (sous classe de Ressource) :
	- longueur {integer}, en minutes

Cette ressemblance fera l'objet d'une traduction par un héritage entre les 3 types de ressources et la classe mère **Ressource**. La classe mère sera considérée comme abstraite.

- **Exemplaire** :
    - codeRessource {integer} {clé étrangère vers **Ressource**}
    - numExemplaire {integer} {local key}
	- etat {énumération : Neuf, Bon, Abime, Perdu}
	- empruntable() vérifie que l'état permet un emprunt et que le livre n'est pas déjà emprunté

Le code de l'oeuvre et le numéro de l'exemplaire seront la clé primaire de cette table. Les exemplaires sont reliés à la ressource par une relation de composition.
Pour savoir si une oeuvre est disponible à l'emprunt ou non, on se référera à la table Emprunt. Un exemplaire qui n'est pas "Neuf" ou "Bon" ne peut plus être emprunté.

- **Contributeur** :
	- nom {string}
	- prenom {string}
	- dateNaissance {date}
	- nationalite {string}

On se servira du nom, prénom ainsi que de la date de naissance (pour éviter les homonymes) d'un contributeur comme clé primaire.
Parmi les **Contributeur**, on distinguera encore une fois différentes classes filles :
- Auteur (sous classe de Contributeur) 
- Compositeur (sous classe de Contributeur)  
- Interprete (sous classe de Contributeur) 
- Realisateur (sous classe de Contributeur) 
- Acteur (sous classe de Contributeur) 

La classe **Contributeur** sera une classe abstraite.

- **ContribueaRessource** (Contributeur - Ressource) :
	- nomContributeur {string} {clé étrangère vers la classe COntributeur}
	- prenomContributeur {string} {clé étrangère vers la classe Contributeur}
	- ressource {integer} {clé étrangère vers la classe Ressource}

- **Utilisateur** :
	- login {string} {key}
	- mdp {string}
	- nom {string}
	- prenom {string}
	- adresse {string}
	- adresseEmail {string} 
	- authentifie {boolean}

- **Personnel**
	
- **Adhérent** :
	- dateNaissance {date}
	- numTelephone {integer(10)} 
	- carteAdherent {boolean}
	- blacklist {boolean}

Le booléen carteAdhérent permet de différencier les adhérents actuels des adhérents passés. Un adhérent avec carteAdhérent étant à 0 ne peut plus accéder aux services.
Un adhérent qui a été blaklisté ne peut plus souscrire à une nouvelle adhésion. 
La clé primaire est le login car on le suppose unique pour chaque utilisateur et immuable.
	
Les utilisateurs seront liés à l'adhérent et au membre du personnel correspondant par un héritage. La classe mère étant l'utilisateur. L'attribut authentifie permet de vérifier
qu'un utilisateur peut emprunter une ressource.

- **Periode** :
	- debut {date}
	- fin{date}

La clé de cette table est la date de début et de fin de la période.

- **PeriodeAdhésion** (Periode - Adhérent) :
	- debutAdhesion {date} {clé étrangère vers Periode}
	- finAdhesion {date} {clé étrangère vers Periode}
	- loginAdherent {string} {clé étrangère vers Adhérent}


Cette table va permettre de répertorier de gérer les différentes adhésions de chaque adhérent.

- **Emprunt** (Adhérent - Exemplaire) :
	- codeExemplaire {integer} {clé étrangère vers Exemplaire}
	- numExemplaire {integer} {clé étrangère vers Exemplaire}
	- loginAdherent {string} {clé étrangère vers Adhérent}
	- datePret {date}
	- duree {integer}, en nombre de jours
	- dateRetour() calcule la date de retour à partir de la date de prêt et de la durée du prêt.

La clé de **Emprunt** sera composé de codeExemplaire, numExemplaire, loginAdherent et datePret afin de 
gérer le cas où un adhérent d'emprunter plusieurs fois la même ressource.

- **Rendu** (Adhérent - Exemplaire) :
    - codeExemplaire {integer} {clé étrangère vers Exemplaire}
	- numExemplaire {integer} {clé étrangère vers Exemplaire}
	- loginAdherent {string} {clé étrangère vers Adhérent}
	- dateRetourEffective {date}
	- etatRendu {énumération : Neuf, Bon, Abime, Perdu}
	- rembourse {boolean} permet de vérifier si l'adhérent ayant perdu une ressource l'a remboursée. La valeur est à 0 si l'adhérent n'a pas besoin de rembourser ou a effectuée le remboursement. La valeur est à 1 si le remboursement n'a pas été encore effectuée.
	- Degradation() permet de vérifier si l'état de la ressource s'est détérioré. On considérera qu'une détérioration grave rend le livre plus empruntable ou que le livre est perdu.

Pour la même raison que dans **Emprunt**, la clé de **Rendu** sera codeExemplaire, numExemplaire, loginAdherent et dateRetourEffective.
Un personnel qui souhaiterait emprunter des livres devra se créer un compte utilisateur en tant qu'adhérent.
L'état dans lequel la ressource est rendu est soit le même que l'état dans lequel elle a été emprunté, soit pire.

- **Suspension** (Adhérent - Personnel) :
	- loginAdherent {string} {clé étrangère vers Adhérent}
	- loginPersonnel {string} {clé étrangère vers Personnel}
	- dateDebut {date}
	- duree {integer}, un nombre de jours

La table **Suspension** permettra de vérifier l'historique des suspensions d'un adhérent pour savoir si il doit être blacklisté.

Toutes les dates seront au format JJ-MM-AAAA.

Pour les livres, on considère que l'ISBN permet la réédition de l'oeuvre. Une décomposition en plusieurs tables permettra donc de gérer l'éventuelle réédition d'un livre. 
Afin de connaître le nombre d'exemplaires disponible d'une oeuvre, on pourra se référer à la table **Exemplaire** qui contient les différents exemplaires d'une oeuvre.
On définira le nombre d'exemplaires maximum empruntable en même temps lors de l'implémentation des recettes.
Afin de connaître le nombre d'emprunts actifs d'un adhérent, on pourra se référer à la table **Emprunt**. On pourra ainsi comparer avec le nombre maximum de livres empruntable en même temps.
Afin de connaître le nombre de suspensions on pourra se référer à la table **Suspension**. 
Le nombre de jours de retard sera calculé en faisant la différence entre la date de retour effective et la date de retour théorique.
La bibliothèque sera la seule juge pour blacklister un adhérent. 
Dans le cas des personnels et des adhérents, nous avons décidé de ne pas choisir comme clé primaire le numéro de téléphone ou l'adresse email malgré leur unicité en raison de leur non-immuabilité.
Les ressources de la bibliothèque sont rangées selon leur catégorie (un genre fantastique, policier, aventure pour les livres par exemple). Par conséquent, le code de classification d'une ressource permettra de la situer selon sa catégorie, permettant notamment d'évaluer les préférences des adhérents selon les catégories de ressources empruntées.
Un adhérent ne peut pas emprunter plus de 3 ressources. 